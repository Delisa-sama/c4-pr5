// x%2-y/z
extern int func(int x, int y, int z);
#include <stdio.h>

int main(int argc, char const *argv[]) {
    printf("121 mod 2 - 9 / 2 = -3 -> %d\n", func(121, 9, 2));
    printf("8 mod 2 - 2 / 3 = 2 -> %d\n", func(8, 2, 3));
    return 0;
}
