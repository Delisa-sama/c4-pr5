FORMAT ELF

; x%2-y/z
public func
func:
  MOV EAX, [4+ESP]  ; EAX = params[0]
  cdq
  MOV ECX, 2   ; ECX = 2
  IDIV ECX  ; mod result in EDX

  MOV EAX, [8+ESP] ;  EAX = params[1]
  cdq
  MOV ECX, [12+ESP] ; ECX = params[2]
  IDIV ECX ; div result in EAX

  MOV ECX, EAX ; ECX = EAX
  MOV EAX, EDX ; EAX = EDX

  SUB EAX, ECX

  ret
